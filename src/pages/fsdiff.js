import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <h1>Future of fsdiff.</h1>
    <p>fsdiff is an awesome tool for comparing local filesystems or images. </p>
    <p>Have a look at the <a href="https://pypi.org/project/fsdiff/">source</a>! </p>
    <Link to="/">...or go back.</Link>
  </div>
)

export default SecondPage
