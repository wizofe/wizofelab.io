import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>wizofe</h1>
    <p>Welcome to my web corner. You may find an eclectic mix of ideas, code, DNA nucleotides, poems and who knows? Maybe music.</p>
    <p>Now, if you may, follow me.</p>
    <Link to="/fsdiff/">fsdiff</Link>
  </div>
)

export default IndexPage
